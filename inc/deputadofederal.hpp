#ifndef DEPUTADOFEDERAL_HPP
#define DEPUTADOFEDERAL_HPP
#include <iostream>
#include <string>
#include "pessoa.hpp"
#include "candidato.hpp"
class DeputadoFederal : public Candidato{
	        public:
                DeputadoFederal();
                ~DeputadoFederal();
                void organizar(Candidato * candidatos, int n);
};


#endif
