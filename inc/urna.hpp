#ifndef URNA_HPP
#define URNA_HPP
#include "../inc/pessoa.hpp"
#include "../inc/candidato.hpp"
#include "../inc/presidente.hpp"
#include "../inc/eleitor.hpp"
#include "../inc/deputadodistrital.hpp"
#include "../inc/deputadofederal.hpp"
#include "../inc/senador.hpp"
#include "../inc/governador.hpp"
#include <iostream>
#include <vector>
using namespace std;

class Urna{
	private:			
		int numero_eleitores;
		string vencedorpresidente;
		string vencedorsenador;
		string vencedordeputadofederal;
		string vencedordeputadodistrital;
		string vencedorgovernador;
		int brancopresidente;
		int brancosenador;
		int brancofederal;
		int brancodistrital;
		int brancogovernador;
		Candidato arquivo1[26];
		Candidato arquivo2[1237];
		Presidente presidente[13];
		vector <Eleitor> eleitor;
		Senador senador[20];
		Governador governador[11];
		DeputadoDistrital deputadodistrital[968];
		DeputadoFederal deputadofederal[185];
	public:
		Urna();
		~Urna();
		void setBrancoPresidente(int brancopresidente);
		int getBrancoPresidente();
		void setBrancoSenador(int brancosenador);
		int getBrancoSenador();
		void setBrancoFederal(int brancofederal);
		int getBrancoFederal();
		void setBrancoDistrital(int brancodistrital);
		int getBrancoDistrital();
		void setBrancoGovernador(int brancogovernador);
		int getBrancoGovernador();
		void setVencedorPresidente(string vencedorpresidente);
		string getVencedorPresidente();
		void setVencedorSenador(string vencedorsenador);
		string getVencedorSenador();
		void setVencedorDeputadoFederal(string vencedordeputadofederal);
		string getVencedorDeputadoFederal();
		void setVencedorDeputadoDistrital(string vencedordeputadodistrital);
		string getVencedorDeputadoDistrital();
		void setVencedorGovernador(string vencedorgovernador);
		string getVencedorGovernador();
		void cabecalho();
		void iniciar();
		void dados();
		void citarDados();
		void listarCandidatos();
		void telaInicial();		
		void telaDados();
		void telaVoto();
		void localizarCandidato(int opcao ,string num_candidato);
		void votar(string voto[], int num_eleitor);
		void telaEleitor();
		void relatorio();
		void contarVotos();
		void fim();		
		
};


#endif
