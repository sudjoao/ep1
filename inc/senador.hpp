#ifndef SENADOR_HPP
#define SENADOR_HPP
#include "candidato.hpp"
#include <string>
using namespace std;
class Senador : public Candidato{
	private:
		string vice2;
	public:
		Senador();
		~Senador();
		void setVice2(string vice2);
		string getVice2();
		void organizar(Candidato * candidatos, int n);

};
#endif
