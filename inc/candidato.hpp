#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP
#include "pessoa.hpp"
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

class Candidato : public Pessoa {
protected:
	string numero;
	string partido;
	string UF;
	string cargo;
	string vice;
	int quantidadeVotos;
	ifstream arquivo;
public:
	Candidato();
	~Candidato();
	void setNumero(string numero);
	string getNumero();
	void setPartido(string partido);
	string getPartido();
	void setUF(string UF);
	string getUF();
	void setCargo(string cargo);
	string getCargo();
	void setVice(string vice);
	string getVice();
	void setQuantidadeVotos(int quantidadeVotos);
	int getQuantidadeVotos();
	void mostrarDados();
	void iniciarArquivo(int numero_arquivo, int n_linha);
	void organizar(Candidato * candidatos, int n);
};

#endif
