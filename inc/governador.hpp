#ifndef GOVERNADOR_HPP
#define GOVERNADOR_HPP
#include <iostream>
#include <string>
#include "candidato.hpp"
using namespace std;
class Governador : public Candidato{
	public:
		Governador();
		~Governador();
		void organizar(Candidato * candidatos, int n);
};


#endif
