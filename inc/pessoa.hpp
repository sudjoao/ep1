#ifndef PESSOA_HPP
#define PESSOA_HPP
#include <string>
using namespace std;
class Pessoa{
protected:
	string nome;
	string cpf;
public:
	Pessoa();
	~Pessoa();
	void setNome(string nome);
	string getNome();
	void setCpf(string cpf);
	string getCpf();
};


#endif
