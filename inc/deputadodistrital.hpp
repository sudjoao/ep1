#ifndef DEPUTADODISTRITAL_HPP
#define DEPUTADODISTRITAL_HPP
#include <iostream>
#include <string>
#include "pessoa.hpp"
#include "candidato.hpp"
class DeputadoDistrital : public Candidato{
	public:
		DeputadoDistrital();
		~DeputadoDistrital();
		void organizar(Candidato * candidatos, int n);

};



#endif
