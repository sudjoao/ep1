#ifndef ELEITOR_HPP
#define ELEITOR_HPP
#include "pessoa.hpp"
using namespace std;
class Eleitor : public Pessoa{
private:
	string presidente;
	string deputado_federal;
	string deputado_distrital;
	string senador;
	string governador;
public:
	Eleitor();
	~Eleitor();
	void setPresidente(string presidente);
	string getPresidente();
	void setDeputadoFederal(string deputado_federal);
	string getDeputadoFederal();
	void setDeputadoDistrital(string deputado_distrital);
	string getDeputadoDistrital();
	void setSenador(string senador);
	string getSenador();
	void setGovernador(string governador);
	string getGovernador();
};

#endif
