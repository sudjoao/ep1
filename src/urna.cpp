#include "../inc/urna.hpp"
#include "../inc/pessoa.hpp"
#include "../inc/candidato.hpp"
#include "../inc/presidente.hpp"
#include "../inc/eleitor.hpp"
#include "../inc/deputadodistrital.hpp"
#include "../inc/deputadofederal.hpp"
#include "../inc/senador.hpp"
#include "../inc/governador.hpp"
#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;

Urna::Urna(){
	Urna::cabecalho();
	vencedorpresidente = "Não houve votos, vai precisar de segundo turno";
	vencedorsenador = "Não houve votos, vai precisar de segundo turno";
	vencedordeputadofederal = "Não houve votos, vai precisar de segundo turno";
	vencedordeputadodistrital= "Não houve votos, vai precisar de segundo turno";
	vencedorgovernador = "Não houve votos, vai precisar de segundo turno";
	brancopresidente=0;
	brancosenador=0;
	brancofederal=0;
	brancodistrital=0;
	brancogovernador=0;
	cout << "Bem vindos as eleições 2018\nRecomenda-se utilizar o terminal em tela maximizada para melhor vizualização.\nPressione enter para continuar";}
Urna::~Urna(){
	Urna::cabecalho();
	cout << "Obrigado por participar, quem decide o futuro do Brasil são vocês!!!!!!\n";}
void Urna::setVencedorPresidente(string vencedorpresidente){
	this->vencedorpresidente = vencedorpresidente;
}
void Urna::setBrancoPresidente(int brancopresidente){
	this->brancopresidente+=brancopresidente;}
int Urna::getBrancoPresidente(){
	return brancopresidente;}
void Urna::setBrancoSenador(int brancosenador){
	this->brancosenador += brancosenador;}
int Urna::getBrancoSenador(){
	return brancosenador;}
void Urna::setBrancoFederal(int brancofederal){
	this->brancofederal+=brancofederal;}
int Urna::getBrancoFederal(){
	return brancofederal;}
void Urna::setBrancoDistrital(int brancodistrital){
	this->brancodistrital+=brancodistrital;}
int Urna::getBrancoDistrital(){
	return brancodistrital;}
void Urna::setBrancoGovernador(int brancogovernador){
	this->brancogovernador+=brancogovernador;}
int Urna::getBrancoGovernador(){
	return brancogovernador;}
string Urna::getVencedorPresidente(){
	return vencedorpresidente;
}
void Urna::setVencedorSenador(string vencedorsenador){
	this->vencedorsenador = vencedorsenador;
}
string Urna::getVencedorSenador(){
	return vencedorsenador;
}
void Urna::setVencedorDeputadoFederal(string vencedordeputadofederal){
	this->vencedordeputadofederal = vencedordeputadofederal;
}
string Urna::getVencedorDeputadoFederal(){
	return vencedordeputadofederal;
}
void Urna::setVencedorDeputadoDistrital(string vencedordeputadodistrital){
	this->vencedordeputadodistrital = vencedordeputadodistrital;
}
string Urna::getVencedorDeputadoDistrital(){
	return vencedordeputadodistrital;
}
void Urna::setVencedorGovernador(string vencedorgovernador){
	this->vencedorgovernador = vencedorgovernador;
}
string Urna::getVencedorGovernador(){
	return vencedorgovernador;
}

void Urna::cabecalho(){
	system("clear");
	cout << "\n --------------------------------------------------------------------------------------------------\n" << "|\t\t\t\t\tEleicoes 2018\t\t\t\t\t           |\n"<< " --------------------------------------------------------------------------------------------------\n\n";
}
void Urna::iniciar(){
	for(int i=0; i<26; i++)
                        arquivo1[i].iniciarArquivo(2, i+1);
	for(int j=0; j<1237; j++)
			arquivo2[j].iniciarArquivo(1, j+1);
}
void Urna::listarCandidatos(){
	int i;
	Urna::cabecalho();
	cout << "Aqui serão listados os candidatos cadastrados nesta urna na seguinte ordem: presidentes, senadores, deputados federais, deputados distritais e governadores.\nPressione enter para continuar";
	getchar();
	Urna::cabecalho();
	for(i=0; i<13; i++)
		presidente[i].mostrarDados();
	cout << "Esses são os candidatos a presidente.\nPressione enter para continuar\n";
	getchar();
	for(i=0; i<20; i++)
		senador[i].mostrarDados();
	cout << "Esses são os candidatos a senador.\nPressione enter para continuar\n";
        getchar();
	Urna::cabecalho();
	for(i=0; i<185; i++)
		deputadofederal[i].mostrarDados();
	cout << "Esses são os candidatos a deputado federal.\nPressione enter para continuar\n";
	getchar();
	Urna::cabecalho();
	for(i=0; i<968; i++)
		deputadodistrital[i].mostrarDados();
	cout << "Esses são os candidatos a deputado distrital.\nPressione enter para continuar\n";
	getchar();
	for(i=0; i<11; i++)
		governador[i].mostrarDados();
	cout << "Esses são os candidatos a governador.\nPressione enter para continuar\n";
        getchar();
	}
void Urna::dados(){
	getchar();
	Urna::cabecalho();
	int i;
	for(i=0; i<968; i++){
		if(i<13)
                presidente[i].organizar(arquivo1, i);
		if(i<11)
			governador[i].organizar(arquivo2, i);
		if(i<20)
			senador[i].organizar(arquivo2, i);
		if(i<185)
			deputadofederal[i].organizar(arquivo2, i);
		deputadodistrital[i].organizar(arquivo2, i);
		}
	Urna::listarCandidatos();
	Urna::cabecalho();
		cout << "Digite a quantidade de eleitores que participarão dessa eleição:\n";
	cin >> numero_eleitores;
	while(0>=numero_eleitores){
		cout << "Digite um numero valido:\n";
		cin >>numero_eleitores;}
	for(int i=0; i<numero_eleitores; i++)
		eleitor.push_back(Eleitor());
}
void Urna::citarDados(){
	Urna::cabecalho();
	for(int i=0; i<13; i++)
	cout << presidente[i].getCargo() <<  ": " << presidente[i].getNome() << endl << "Vice:" << presidente[i].getVice() << endl << presidente[i].getNumero() << endl << presidente[i].getPartido() << endl << endl;}

void Urna::telaInicial(){
	if(numero_eleitores>0){
	Urna::cabecalho();
	cout << "Agora vamos começar a votação.\nPressione enter para continuar";
	Urna::telaVoto();}
}





void Urna::telaVoto(){
	string nome, cpf, voto[5];
	for(int i=0; i<numero_eleitores; i++){
		getchar();
		Urna::cabecalho();
		cout << "Digite seu nome:\n";
		getline(cin, nome);
		cout << "O nome digitado foi: " << nome << " digite novamente para confirmar ou corrija para o certo caso esteja errado\n";
		getline(cin,nome);
		cout << "Digite o seu CPF:\n";
		cin>>cpf;
		cout << "O CPF digitado foi: " << cpf << " digite novmaente para confirmar ou corrija para o certo caso esteja errado\n";
		cin>>cpf;
		eleitor[i].setNome(nome);
		eleitor[i].setCpf(cpf);
		for(int j=0; j<5; j++){
			Urna::cabecalho();	
			cout << "Digite seu voto para ";
			if(j==0)
				cout << "presidente\n";
			else if(j==1)
                                cout << "senador\n";
			else if(j==2)
				cout << "deputado federal\n";
			else if(j==3)
				cout << "deputado distrital\n";
			else
				cout << "governador\n";
			cin >> voto[j];
			Urna::localizarCandidato(j, voto[j]);
			cout << "Seu voto foi: " << voto[j] << " digite novamente para confirmar ou digite outro numero para votar caso queira mudar:\n(Lembre-se que voto 0 conta como em branco e um numero não cadastrado na urna sera considerado como nulo)\n";
			cin >> voto[j];
			}
		Urna::votar(voto, i);
		}
	Urna::relatorio();


}
void Urna::votar(string voto[], int num_eleitor){
	int i, cont=0;        
	for(i=0; i<5; i++){
		if(voto[i]=="0" && i==0)
			Urna::setBrancoPresidente(1);
		else if(voto[i]=="0" && i==1)
			Urna::setBrancoSenador(1);
		else if(voto[i]=="0" && i==2)
			Urna::setBrancoFederal(1);
		else if(voto[i]=="0" && i==3)
			Urna::setBrancoDistrital(1);
		else if(voto[i]=="0" && i==4)
			Urna::setBrancoGovernador(1);
	}
	for(i=0; i<13; i++){
                 if(presidente[i].getNumero()==voto[0]){
				presidente[i].setQuantidadeVotos(presidente[i].getQuantidadeVotos()+1);
				eleitor[num_eleitor].setPresidente(presidente[i].getNome());
				cont++;
			}
		 if(cont==0 && i==12)
			 eleitor[num_eleitor].setPresidente("Branco");
	}
	cont=0;
	 for(i=0; i<20; i++){
                 if(senador[i].getNumero()==voto[1]){
                                senador[i].setQuantidadeVotos(senador[i].getQuantidadeVotos()+1);
                                eleitor[num_eleitor].setSenador(deputadodistrital[i].getNome());
                                        }
		 if(cont==0 && i==19)
			 eleitor[num_eleitor].setSenador("Branco");
	}
	 cont=0;
	for(i=0; i<185; i++){
                 if(deputadofederal[i].getNumero()==voto[2]){
				deputadofederal[i].setQuantidadeVotos(deputadofederal[i].getQuantidadeVotos()+1);
				eleitor[num_eleitor].setDeputadoFederal(deputadofederal[i].getNome());
			}
		if(cont==0 && i==184)
			eleitor[num_eleitor].setDeputadoFederal("Branco");
	}
	cont=0;
	for(i=0; i<968; i++){
                 if(deputadodistrital[i].getNumero()==voto[3]){
				deputadodistrital[i].setQuantidadeVotos(deputadodistrital[i].getQuantidadeVotos()+1);
				eleitor[num_eleitor].setDeputadoDistrital(deputadodistrital[i].getNome());
					}
		 if(cont==0 && i==967)
			 eleitor[num_eleitor].setDeputadoDistrital("Branco");
	}
	cont=0;
	 for(i=0; i<11; i++){
                 if(governador[i].getNumero()==voto[4]){
                                governador[i].setQuantidadeVotos(governador[i].getQuantidadeVotos()+1);
                                eleitor[num_eleitor].setGovernador(governador[i].getNome());
                                        }
		 if(cont==0 && i==10)
			 eleitor[num_eleitor].setGovernador("Branco");
	 }

}
void Urna::localizarCandidato(int opcao, string num_candidato){
	int i, cont=0;
	if(opcao==0){
		for(i=0; i<13; i++)
			if(presidente[i].getNumero()==num_candidato){
				cout << "O numero digitado corresponde ao candidato " << presidente[i].getNome() << endl;
				cont++;}
	}
	else if(opcao==1){
		for(i=0; i<20; i++)
			if(senador[i].getNumero()==num_candidato){
				cout << "O numero digitado corresponde ao candidato " << senador[i].getNome() << endl;
				cont++;}
	}
	else if(opcao==2){
		for(i=0; i<185; i++)
			if(deputadofederal[i].getNumero()==num_candidato){
				cout << "O numero digitado corresponde ao candidato " << deputadofederal[i].getNome() << endl;
				cont++;}
	}
	else if(opcao==3){
		for(i=0; i<968; i++)
			if(deputadodistrital[i].getNumero()==num_candidato){
				cout << "O numero digitado corresponde ao candidato " << deputadodistrital[i].getNome() << endl;
				cont++;}
	}
	else{
		for(i=0; i<11; i++)
			if(governador[i].getNumero()==num_candidato){
				cout << "O numero digitado corresponde ao candidato " << governador[i].getNome() << endl;
				cont ++;}
	}
	if(cont ==0)
		cout << "O voto digitado é nulo/branco" << endl;
}
void Urna::relatorio(){
	Urna::cabecalho();
	for(int i=0; i<numero_eleitores; i++){
		cout << "Nome do Eleitor: " << eleitor[i].getNome() << endl << "CPF do eleitor: " << eleitor[i].getCpf() << endl << "Escolha para presidente: " << eleitor[i].getPresidente() << endl << "Escolha para senador: " << eleitor[i].getSenador() << endl << "Escolha para deputado federeal: " << eleitor[i].getDeputadoFederal() << endl << "Escolha para deputado distrital: " << eleitor[i].getDeputadoDistrital() << endl << "Escolha para governador: " << eleitor[i].getGovernador() << endl << endl;}
	cout << "Pressione enter para continuar\n";
	getchar();
	Urna::contarVotos();
}
void Urna::contarVotos(){
	int votos[5]={-1, -1, -1, -1, -1};
	int j;
	for(j=0; j<968; j++){
		if(j<11){
			if(governador[j].getQuantidadeVotos()>votos[0] && governador[j].getQuantidadeVotos()>=1){
                                   votos[0]=governador[j].getQuantidadeVotos();
				   vencedorgovernador = governador[j].getNome();}
		else if(governador[j].getQuantidadeVotos()==votos[0])
		       vencedorgovernador = "Houve empate, vai precisar de segundo turno";
		}
		if(j<13){
			if(presidente[j].getQuantidadeVotos()>votos[1] && presidente[j].getQuantidadeVotos()>=1){
				votos[1]=presidente[j].getQuantidadeVotos();
				vencedorpresidente = presidente[j].getNome();}
		else if(presidente[j].getQuantidadeVotos()==votos[1])
			vencedorpresidente = "Houve empate, vai precisar de segundo turno";
		}
		if(j<20){
			if(senador[j].getQuantidadeVotos()>votos[2] && senador[j].getQuantidadeVotos()>=1){
                              votos[2]=senador[j].getQuantidadeVotos();
                              vencedorsenador = senador[j].getNome();}
			else if(senador[j].getQuantidadeVotos()==votos[2])
 				vencedorsenador = "Houve empate, vai precisar de segundo turno";
			}
		if(j<185){
			if(deputadofederal[j].getQuantidadeVotos()>votos[3] && deputadofederal[j].getQuantidadeVotos()>=1){
       		                 votos[3]=deputadofederal[j].getQuantidadeVotos();
                                 vencedordeputadofederal = deputadofederal[j].getNome();}
                        else if(deputadofederal[j].getQuantidadeVotos()==votos[3])
                                 vencedordeputadofederal = "Houve empate, vai precisar de segundo turno";
			}
		if(deputadodistrital[j].getQuantidadeVotos()>votos[4]&& deputadodistrital[j].getQuantidadeVotos()>=1){
			 votos[4]=deputadodistrital[j].getQuantidadeVotos();
			 vencedordeputadodistrital = deputadodistrital[j].getNome();
		}
		else if(deputadodistrital[j].getQuantidadeVotos()==votos[4])
			 vencedordeputadodistrital = "Houve empate, vai precisar de segundo turno";
	}
	Urna::fim();
}

void Urna::fim(){
	getchar();
	Urna::cabecalho();
	cout << "O vencedor para o cargo de presidente foi: " << getVencedorPresidente() << endl << "Houve(ram) " << Urna::getBrancoPresidente() << " voto(s) em branco"<< endl << "O vencedor para o cargo de senador foi: " << getVencedorSenador() << endl << "Houve(ram) " << Urna::getBrancoSenador() << " voto(s) em branco" << endl << "O vencedor para o cargo de deputado federal foi: " << getVencedorDeputadoFederal() << endl << "Houve(ram) " << Urna::getBrancoFederal() << " voto(s) em branco" << endl <<  "O vencedor para o cargo de deputado distrital foi: " << getVencedorDeputadoDistrital() << endl << "Houve(ram) " << Urna::getBrancoDistrital() << " voto(s) em branco" << endl << "O vencedor para o cargo de governador foi: " << getVencedorGovernador() << endl << "Houve(ram) " << Urna::getBrancoGovernador() << " voto(s) em branco"<< endl << endl<< "Pressione enter para continuar\n";
	getchar();
	}
