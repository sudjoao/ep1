#include "../inc/candidato.hpp"
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

Candidato::Candidato(){
	UF = "BR";
	partido = "PT";
	numero = "13";}

Candidato::~Candidato(){}
void Candidato::setNumero(string numero){
	this->numero = numero;}

string Candidato::getNumero(){
	return numero;}

void Candidato::setPartido(string partido){
	this->partido = partido;}

string Candidato::getPartido(){
	return partido;}

void Candidato::setUF(string UF){
	this->UF = UF;}

string Candidato::getUF(){
	return UF;}

void Candidato::setCargo(string cargo){
	this->cargo = cargo;}

string Candidato::getCargo(){
	return cargo;}

void Candidato::setVice(string vice){
	this->vice = vice;}

string Candidato::getVice(){
	return vice;}

void Candidato::setQuantidadeVotos(int quantidadeVotos){
	this->quantidadeVotos = quantidadeVotos;}
int Candidato::getQuantidadeVotos(){
	return quantidadeVotos;}
void Candidato::iniciarArquivo(int numero_arquivo, int n_linha){
	string linha, dado1, dado2, dado3, dado4, dado5, dado6;
       	int i, contador_ponto=0, contador_linha=0;

		if(numero_arquivo==1)
			arquivo.open("data/ArquivoOutros.csv");
	        else if(numero_arquivo==2)
			arquivo.open("data/ArquivoPresidente.csv");
		while(!arquivo.eof() && contador_linha<=n_linha){
        		getline(arquivo, linha);
			contador_linha++;}
        	for(i=0; i<linha.size(); i++){
                	if(linha[i]==';')
                	        contador_ponto++;
                	else if(contador_ponto==10 && linha[i]!='"')
                       		dado1.push_back(linha[i]);
                	else if(contador_ponto==14 && linha[i]!='"')
                        	dado2.push_back(linha[i]);
			else if(contador_ponto==16 && linha[i]!='"')
				dado3.push_back(linha[i]);
			else if(contador_ponto==18 && linha[i]!='"')
				dado4.push_back(linha[i]);
			else if(contador_ponto==20 && linha[i]!='"')
				dado5.push_back(linha[i]);
			else if(contador_ponto==29 && linha[i]!='"')
				dado6.push_back(linha[i]);
			else if(contador_ponto>29)
				break;}
		setUF(dado1);
		setCargo(dado2);
		setNumero(dado3);
		setNome(dado4);
		setCpf(dado5);
		setPartido(dado6);
		arquivo.close();}

void Candidato::mostrarDados(){
	cout << "Nome do Candidato: " << nome << endl << "Partido do Candidato: " << partido << endl << "Número do Candidato: " << numero << endl << "Cargo do Candidato: " << cargo << endl << "UF de Abrangencia: " << UF << endl << endl;}
