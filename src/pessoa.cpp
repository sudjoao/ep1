#include <iostream>
#include "../inc/pessoa.hpp"
using namespace std;

Pessoa::Pessoa(){
	nome = "Alguem";
	cpf = "000000000";
}

Pessoa::~Pessoa(){
}

void Pessoa::setNome(string nome){
	this->nome = nome;}

string Pessoa::getNome(){
	return nome;}

void Pessoa::setCpf(string cpf){
	this->cpf = cpf;}

string Pessoa::getCpf(){
	return cpf;}
