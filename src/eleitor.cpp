#include "../inc/eleitor.hpp"
#include <iostream>
#include <string>
using namespace std;
Eleitor::Eleitor(){
	presidente = "Nulo";
	deputado_federal="Nulo";
	deputado_distrital="Nulo";
	senador="Nulo";
	governador="Nulo";}

Eleitor::~Eleitor(){
}
void Eleitor::setPresidente(string presidente){
	this->presidente = presidente;
}
string Eleitor::getPresidente(){
	return presidente;
}
void Eleitor::setDeputadoFederal(string deputado_federal){
	this->deputado_federal = deputado_federal;
}
string Eleitor::getDeputadoFederal(){
	return deputado_federal;
}
void Eleitor::setDeputadoDistrital(string deputado_distrital){
	this->deputado_distrital = deputado_distrital;
}
string Eleitor::getDeputadoDistrital(){
	return deputado_distrital;
}
void Eleitor::setSenador(string senador){
	this->senador = senador;
}
string Eleitor::getSenador(){
	return senador;
}
void Eleitor::setGovernador(string governador){
	this->governador = governador;
}
string Eleitor::getGovernador(){
	return governador;
}

