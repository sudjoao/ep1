# Exercício de Programação 1

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricao)

## Como usar o projeto

* Compile o projeto com o comando:

```sh
make
```

* Execute o projeto com o comando:

```sh
make run
```
* Digite enter quando estiver pedindo
* Digite o numero de eleitores
* Digite o nome e cpf do eleitor
* Digite os votos na seguinte ordem, presidente, senador, deputado federal, deputado distrital e governador(votos igual a 0 serão considerados em branco, e dos numeros não cadastrados serão considerados como nulos)
## Funcionalidades do projeto
```sh
1- Mostrar tela com os dados de todos os candidatos disponiveis no arquivo;
2- Receber a quantidade de eleitores caso forem igual ou menor a zero ler novamente;
3- Receber os dados dos eleitores(nome e cpf) duas vezes caso o eleitor queira alterar algum dado;
4- Receber os votos para cada candidato 2x caso o eleitor queira modificar o voto;
5- Mostrar o relatorio de votos mostrando o nome e cpf dos eleitores e seus votos, dizendo se foi nulo ou apresentando o nome do candidato;
6- Mostrar quem venceu, no caso os candidatos que obtiveram o maior numero de votos.
```

## Bugs e problemas
* Por estar utilizando a função "system("clear")" talvez dê erro em sistemas operacionais que não forem Linux
## Referências
